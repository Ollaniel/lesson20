package by.train.java.xml.stax;

public enum NodeNames {
	GOODS,
	GOOD,
	NAME,
	DESCRIPTION,
	CUSTOMER;
	
	public static NodeNames getName(String e) {		
		switch (e) {
		case "goods":
			return GOODS;
		case "good":
			return GOOD;
		case "name":
			return NAME;
		case "description":
			return DESCRIPTION;
		case "customer":
			return CUSTOMER;
		default:
			throw new EnumConstantNotPresentException(NodeNames.class, e);
		}
	}	
}
